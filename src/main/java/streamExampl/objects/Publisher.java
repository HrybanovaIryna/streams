package streamExampl.objects;

import lombok.Data;

import java.util.List;

@Data
public class Publisher {
    private String name;
    private List<PaperEdition> paperEditions;

}
