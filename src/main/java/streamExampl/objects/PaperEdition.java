package streamExampl.objects;

import java.util.List;

public interface PaperEdition {

    String getName();
    int getNumber();
    int getYear();
    long getPrice();
    Status getStatus();
    List<String> getChapters();

    void setStatus(Status status);
    void setPrice(long price);
}
