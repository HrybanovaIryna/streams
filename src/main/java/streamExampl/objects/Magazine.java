package streamExampl.objects;

import lombok.Data;

import java.util.List;

@Data
public class Magazine implements PaperEdition{
    private String name;
    private int number;
    private int year;
    private long price;
    private Status status;
    private List<String> chapters;
}
