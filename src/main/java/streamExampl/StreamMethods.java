package streamExampl;

import streamExampl.objects.Magazine;
import streamExampl.objects.PaperEdition;
import streamExampl.objects.Publisher;
import streamExampl.objects.Status;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StreamMethods {

    public List<PaperEdition> filterByName(Publisher publisher, String name) {
        return publisher.getPaperEditions().stream()
                .filter(paperEdition -> Objects.nonNull(paperEdition.getName()) && paperEdition.getName().equals(name))
                .collect(Collectors.toList());

    }

    public List<PaperEdition> filterFromTo(Publisher publisher, int numberFrom, int numberTo) {
        return publisher.getPaperEditions().stream()
                .filter(paperEdition -> (paperEdition.getClass() == Magazine.class
                        && paperEdition.getNumber() >= numberFrom
                        && paperEdition.getNumber() <= numberTo))
                .collect(Collectors.toList());
    }

    public List<PaperEdition> skipElements(Publisher publisher, long n) {
        return publisher.getPaperEditions().stream()
                .skip(n)
                .collect(Collectors.toList());
    }

    public List<PaperEdition> distinct(Publisher publisher) {
        return publisher.getPaperEditions().stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public List<String> mapMagazine(Publisher publisher, String name) {
        return publisher.getPaperEditions().stream()
                .filter(paperEdition -> Objects.nonNull(paperEdition.getName()) && paperEdition.getName().equals(name))
                .map(paperEdition -> paperEdition.getName() + " " + paperEdition.getNumber() + "/" + paperEdition.getYear())
                .collect(Collectors.toList());
    }

    public List<PaperEdition> peekToUpperCase(Publisher publisher, Status status) {
        return publisher.getPaperEditions().stream()
                .peek(paperEdition -> paperEdition.setStatus(status))
                .collect(Collectors.toList());
    }

    public List<PaperEdition> limit(Publisher publisher, int n, int year) {
        return publisher.getPaperEditions().stream()
                .filter(paperEdition -> paperEdition.getYear() == year)
                .limit(n)
                .collect(Collectors.toList());
    }

    public List<PaperEdition> sorted(Publisher publisher) {
        return publisher.getPaperEditions().stream()
                .sorted(Comparator.comparing(PaperEdition::getYear)
                        .thenComparingInt(PaperEdition::getNumber))
                .collect(Collectors.toList());
    }

    public long[] mapToLong(Publisher publisher, long priceFrom) {
        return publisher.getPaperEditions().stream()
                .filter(paperEdition -> paperEdition.getPrice() == priceFrom)
                .mapToLong(PaperEdition::getPrice)
                .toArray();
    }

    public List<String> flatMap(Publisher publisher) {
        return publisher.getPaperEditions().stream()
                .flatMap(paperEdition -> paperEdition.getChapters().stream())
                .collect(Collectors.toList());
    }

    public String findFirst(Publisher publisher) {
        return flatMap(publisher).stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException("No first element"));
    }

    public String findAny(Publisher publisher) {
        return flatMap(publisher).stream()
                .findAny()
                .orElseThrow(() -> new RuntimeException("No any element"));
    }

    public long count(Publisher publisher) {
        return flatMap(publisher).stream()
                .count();
    }

    public boolean anyMatch(Publisher publisher, Status status) {
        return publisher.getPaperEditions().stream()
                .anyMatch(paperEdition -> Objects.nonNull(status) && status.equals(paperEdition.getStatus()));
    }

    public boolean allMatch(Publisher publisher, Status status) {
        return publisher.getPaperEditions().stream()
                .allMatch(paperEdition -> Objects.nonNull(status) && status.equals(paperEdition.getStatus()));
    }

    public PaperEdition min(Publisher publisher) {
        return publisher.getPaperEditions().stream()
                .min(Comparator.comparingInt(PaperEdition::getYear))
                .orElseThrow(() -> new RuntimeException("No min paper edition"));
    }

    public PaperEdition max(Publisher publisher) {
        return publisher.getPaperEditions().stream()
                .max(Comparator.comparingLong(PaperEdition::getPrice))
                .orElseThrow(() -> new RuntimeException("No max price"));
    }

    public void forEach(Publisher publisher, long extraPrice) {
        publisher.getPaperEditions()
                .forEach(paperEdition -> paperEdition.setPrice(paperEdition.getPrice() + extraPrice));
    }

    public Integer[] toArray(Publisher publisher) {
        return publisher.getPaperEditions().stream()
                .map(PaperEdition::getNumber)
                .toArray(Integer[]::new);
    }

    public long reduce(Publisher publisher) {
        return publisher.getPaperEditions().stream()
                .map(PaperEdition::getPrice)
                .reduce((p1, p2) -> p1 + p2)
                .orElse(0L);
    }

    public long sum(Publisher publisher) {
        return publisher.getPaperEditions().stream()
                .mapToLong(PaperEdition::getPrice)
                .sum();
    }

    public double average(Publisher publisher) {
        return publisher.getPaperEditions().stream()
                .mapToLong(PaperEdition::getPrice)
                .average()
                .orElse(0.0);
    }
}
